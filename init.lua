local dancers={}

nodecore.register_playerstep({
		label = "treedance",
		action = function(player, data)
			local name = player:get_player_name()
			local dat=dancers[name] or {}
			dancers[name]=dat
			local oldsneak=dat.sneak
			local newsneak=data.control.sneak
			local newjump=data.control.jump
			if newsneak ~= oldsneak then
				dat.sneak=data.control.sneak
				dat.movetime=nodecore.gametime
			end
			if newjump then
				dat.jump=data.control.jump
				dat.movetime=nodecore.gametime
			end
			if nodecore.gametime == dat.movetime then
				if not dat.dance then
				end
				dat.dance=dat.dance or nodecore.gametime
			end
			if nodecore.gametime-dat.movetime >= 2 then
				if dat.dance then
				end
				dat.dance=false
			end
			dat.pos=player:get_pos()
		end
	})

local oldsoil
local function tree_soil_rate(pos)
	if not nc_skyrealm or nc_skyrealm.in_sky_realm(pos) then
		for k,v in pairs(dancers) do
			if not minetest.get_player_by_name(k) then
				v=nil
				dancers[k]=nil
			else
				if v.dance and nodecore.gametime-v.dance >= 3 and ((not v.tree) or nodecore.gametime-v.tree >= 5) then
					local dist=vector.distance(pos,v.pos)
					if dist < 3 then
						v.tree=nodecore.gametime
						print("boost!",minetest.pos_to_string(pos))
						return oldsoil(pos)*2500
					else
					end
				else
				end
			end
		end
	end
	return oldsoil(pos)
end
local meta=getmetatable(nodecore)
local nc_newindex=meta.__newindex
meta.__newindex=function(t,k,v)
	if k=="tree_soil_rate" then
		oldsoil=v
		meta.__newindex=nc_newindex
		return nc_newindex(t,k,tree_soil_rate)
	end
	return nc_newindex(t,k,v)
end
